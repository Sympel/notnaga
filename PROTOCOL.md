# Anagon Engine Communication Protocol (AECP)

Transferred messages are formatted in the direction AI <-> Client. All
values are sent on seperate lines.

## Preferences

Communication starts by the ai sending their preferences to the client.

- -> `coords`: `qrs` (default) or `xyoddy` — The coordinate system all communication should use.
- -> `endprefs`: Finish sendings preferences.

### Coordinate system

- XYoddY: coordinates are formatted `x y`. 
```
00[]20 -> x
 01[]
02[][]
 [][]  
 |
 v
 y
```
- QRS: coordinates are formatted `q r s`.
```
  []qq[]
 [][][][]
[][]00[][]
 ss[][]rr
  [][][]
```


## Setup

Then, the client sends the whole board state and the side the ai should play on.

+ <- `board`: 

## Examples

A typical communication between an ai and the client may look like this:

```
-> prefs
-> coords
-> <preferred coordinate system>
-> endprefs
# prefs complete
<- setup
<- board
<- 19
<- <tile>
<- <tile>
<- <tile
<- [...]
<- <tile>
<- red
<- 3
<- <tile>
<- [...]
<- black
<- 3
<- [...]
<- turn
<- red
<- side
<- black
<- endsetup
# setup complete 
<- tile
<- <from>
<- <to>
<- piece
<- <from>
<- <to>
-> tile
-> <from>
-> <to>
-> piece
-> <from>
-> <to>
[...]
<- piece
<- <from>
<- <to>
-> forfeit
<- exit
```
