{
  description = "Implementation of the Nonaga board game.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        inherit (pkgs) lib;
      in
      {
        devShells.default = pkgs.mkShell rec {
          # Extra inputs can be added here; cargo and rustc are provided by default.
          packages = with pkgs; [
            udev alsa-lib vulkan-loader
            libxkbcommon wayland # To use the wayland feature

            pkg-config
          ];

          LD_LIBRARY_PATH = lib.makeLibraryPath packages;
        };
      });
}

