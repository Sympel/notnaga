use bevy::prelude::*;
use crossbeam_channel::{unbounded, Receiver, RecvError, SendError, Sender};
use itertools::Itertools;
use std::{
    ffi::OsStr,
    io::{BufRead, BufReader, Write},
    process::{Child, ChildStdin, ChildStdout, Command, Stdio},
    str::FromStr,
    thread::{self},
};
use thiserror::Error;

use crate::{
    board::{Board, Move, Player},
    tile::Tile,
};

pub struct AiPlugin;

impl Plugin for AiPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_systems(Update, ai_actions);
    }
}

fn ai_actions(
    player: Res<Player>,
    mut board: ResMut<Board>,
    mut ais: Query<(&mut Ai, &Player)>,
    mut moves: EventWriter<Move>,
) {
    for mut ai in ais.iter_mut() {
        if *ai.1 != *player {
            continue;
        }

        for response in ai.0.responses() {
            match response {
                AiResponse::Status(status) => match status {
                    Status::Ok => (),
                    Status::Unsupported => error!("features not supported by ai"),
                    Status::Failed => error!("ai encountered an error"),
                },
                AiResponse::Move(mov) => {
                    if board.is_valid(*player, mov) {
                        board.apply(mov).unwrap();
                        moves.send(mov);
                    } else {
                        error!("ai tried to perform invalid move");
                    }
                }
                AiResponse::Eval(eval) => info!("ai evaluation {eval}"),
                AiResponse::Forfeit => info!("ai forfeited"),
            }
        }
    }
}

#[derive(Debug, Component)]
pub struct Ai {
    output: ChildStdin,
    responses: Receiver<AiResponse>,
    prefs: AiPrefs,
}

#[derive(Debug)]
pub struct AiReceiver {
    process: Child,
    input: BufReader<ChildStdout>,
    responses: Sender<AiResponse>,
    prefs: AiPrefs,
}

impl AiReceiver {
    fn run(&mut self) -> Result<()> {
        self.prefs = AiPrefs::get(&mut self.input)?;
        let mut label = String::new();
        let lines = (&mut self.input).lines();
        lines.process_results(|mut lines| -> Result<()> {
            let mut line = move || lines.next().ok_or(Error::Connection { src: None });
            loop {
                match label.as_str() {
                    "piece" | "tile" => {
                        let from = self.prefs.coords.parse_tile(line()?.trim())?;
                        let to = self.prefs.coords.parse_tile(line()?.trim())?;
                        {
                            self.responses.send(AiResponse::Move(Move::new(from, to)))?;
                        }
                    }
                    "" => match line()?.trim() {
                        "ok" => self.responses.send(AiResponse::Status(Status::Ok))?,
                        "unsupported" => self
                            .responses
                            .send(AiResponse::Status(Status::Unsupported))?,
                        "error" => self.responses.send(AiResponse::Status(Status::Failed))?,
                        "forfeit" => self.responses.send(AiResponse::Forfeit)?,
                        l => {
                            label.clear();
                            label.push_str(l);
                        }
                    },
                    _ => unreachable!(),
                }
            }
        })??;
        Ok(())
    }
}

#[derive(Debug)]
pub enum AiCommand {
    Status(Status),
    OppMove(Move),
    Undo,
    Move,
    Eval,
    Exit,
}

#[derive(Debug)]
pub enum AiResponse {
    Status(Status),
    Move(Move),
    Eval(f32),
    Forfeit,
}

#[derive(Debug, Clone, Copy)]
pub enum Status {
    Ok,
    Unsupported,
    Failed,
}

impl Ai {
    pub fn new(cmd: &OsStr) -> Result<Self> {
        let mut process = Command::new(cmd)
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn()?;

        let input = BufReader::new(
            process
                .stdout
                .take()
                .ok_or(Error::Connection { src: None })?,
        );
        let output = process
            .stdin
            .take()
            .ok_or(Error::Connection { src: None })?;

        let (s, r) = unbounded();

        let mut ai = AiReceiver {
            process,
            input,
            responses: s,
            prefs: AiPrefs::default(),
        };

        thread::spawn(move || ai.run());

        Ok(Ai {
            responses: r,
            output,
            prefs: AiPrefs::default(),
        })
    }

    pub fn setup(&mut self, board: &Board, turn: Player, side: Player) -> Result<()> {
        let output = &mut self.output;
        writeln!(output, "setup")?;
        writeln!(output, "board")?;
        writeln!(output, "{}", board.tile_count())?;
        for tile in board.tiles() {
            writeln!(output, "{}", self.prefs.coords.format_tile(tile))?;
        }
        for player in Player::both() {
            writeln!(output, "{player}")?;
            writeln!(output, "{}", board.piece_count(player))?;
            for piece in board.pieces(player) {
                writeln!(output, "{}", self.prefs.coords.format_tile(piece))?;
            }
        }
        writeln!(output, "turn")?;
        writeln!(output, "{turn}")?;
        writeln!(output, "side")?;
        writeln!(output, "{side}")?;
        writeln!(output, "endsetup")?;
        Ok(())
    }

    pub fn responses(&mut self) -> impl Iterator<Item = AiResponse> + '_ {
        self.responses.try_iter()
    }
}

#[derive(Default, Debug, Clone)]
struct AiPrefs {
    coords: CoordinateSystem,
}

impl AiPrefs {
    fn get(input: &mut impl BufRead) -> Result<Self> {
        input.lines().process_results(|mut iter| {
            let mut next_line = || iter.next().ok_or(Error::Connection { src: None });
            let mut prefs = AiPrefs::default();
            loop {
                match next_line()?.trim() {
                    "coords" => {
                        prefs.coords = next_line()?.parse()?;
                    }
                    "endprefs" => break,
                    label => {
                        return Err(Error::Label {
                            label: label.to_owned(),
                        })
                    }
                }
            }
            Ok(prefs)
        })?
    }
}

#[derive(Debug, Default, Copy, Clone)]
enum CoordinateSystem {
    XYOddX,
    #[default]
    QRS,
}

impl CoordinateSystem {
    fn format_tile(self, tile: Tile) -> String {
        match self {
            CoordinateSystem::XYOddX => {
                let (x, y) = tile.to_grid();
                format!("{x} {y}")
            }
            CoordinateSystem::QRS => {
                format!("{} {} {}", tile.q(), tile.r(), tile.s())
            }
        }
    }

    fn parse_tile(self, tile: &str) -> Result<Tile> {
        match self {
            CoordinateSystem::XYOddX => {
                let mut xy = tile.split_whitespace().take(2).map(str::parse);
                let x = xy.next().ok_or(Error::Value)??;
                let y = xy.next().ok_or(Error::Value)??;
                Ok(Tile::from_grid(x, y))
            }
            CoordinateSystem::QRS => {
                let mut qrs = tile.split_whitespace().take(2).map(str::parse);
                let q = qrs.next().ok_or(Error::Value)??;
                let r = qrs.next().ok_or(Error::Value)??;
                Ok(Tile::new(q, r))
            }
        }
    }
}

impl FromStr for CoordinateSystem {
    type Err = Error;

    fn from_str(s: &str) -> std::prelude::v1::Result<Self, Self::Err> {
        match s {
            "xyoddx" => Ok(Self::XYOddX),
            "qrs" => Ok(Self::QRS),
            _ => Err(Error::Value),
        }
    }
}

type Result<T> = std::result::Result<T, Error>;

#[derive(Error, Debug)]
pub enum Error {
    #[error("unexpected packet label \"{label}\"")]
    Label { label: String },
    #[error("invalid packet value")]
    Value,
    #[error("invalid integer in value")]
    IntValue {
        #[from]
        src: std::num::ParseIntError,
    },
    #[error("communication failed")]
    Connection {
        #[from]
        src: Option<std::io::Error>,
    },
    #[error("ai message could not be sent to client")]
    ChannelSend {
        #[from]
        send: SendError<AiResponse>,
    },
    #[error("ai message could not be received")]
    ChannelRecv {
        #[from]
        recv: Option<RecvError>,
    },
    #[error(transparent)]
    Unknown {
        #[from]
        src: anyhow::Error,
    },
}
