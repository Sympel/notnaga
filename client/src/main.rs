pub mod ai;
pub mod board;
pub mod cursor;
pub mod game;
pub mod input;
pub mod tile;

use std::ffi::OsString;

use ai::Ai;
use bevy::{prelude::*, render::camera::ScalingMode, window::PrimaryWindow};
use board::{Board, Player};
use clap::Parser;
use cursor::CursorPlugin;
use game::GamePlugin;
use input::InputPlugin;

/// Game based on the Nonaga board game.
#[derive(Parser, Debug)]
#[command(version, about)]
struct Args {
    /// AI commands to use for the red player.
    #[arg(short, long)]
    red: Option<OsString>,

    /// AI commands to use for the black player.
    #[arg(short, long)]
    black: Option<OsString>,
}

fn main() {
    let args = Args::parse();

    App::new()
        .add_plugins(DefaultPlugins.set(ImagePlugin::default_nearest()))
        .add_plugins(CursorPlugin)
        .add_plugins(InputPlugin)
        .add_plugins(GamePlugin)
        .init_state::<GameState>()
        .insert_resource(ClearColor(Color::rgb(0.6, 0.55, 0.5)))
        .add_systems(Startup, setup_players(args.red, args.black))
        .add_systems(Startup, load_board)
        .add_systems(Startup, setup)
        .add_systems(FixedUpdate, camera_follow)
        .run();
}

fn setup_players(red: Option<OsString>, black: Option<OsString>) -> impl Fn(Commands) {
    move |mut commands: Commands| {
        let cmds = [&red, &black];
        for player in Player::both() {
            if let Some(cmd) = &cmds[player as usize] {
                let ai = Ai::new(&cmd);
                match ai {
                    Ok(ai) => {
                        commands.spawn((player, ai));
                    }
                    Err(error) => {
                        error!("ai for {player} could not be loaded: {error}");
                        commands.spawn((player, Human));
                    }
                }
            } else {
                commands.spawn((player, Human));
            }
        }
    }
}

#[derive(Debug, Component)]
struct Human;

#[derive(Debug, Resource)]
struct Players([Entity; 2]);

#[derive(Default, Debug, Clone, Copy, Resource, States, Hash, PartialEq, Eq)]
enum GameState {
    Setup,
    #[default]
    Play,
    Edit,
    Victory,
}

fn load_board(mut commands: Commands) {
    commands.add(Board::default().load());
}

fn setup(mut commands: Commands, mut window: Query<&mut Window, With<PrimaryWindow>>) {
    window.single_mut().set_maximized(true);
    let mut camera = Camera2dBundle::new_with_far(1000.0);
    camera.projection.scaling_mode = ScalingMode::FixedVertical(10.0);
    commands.spawn(camera);
}

fn camera_follow(
    board: Res<Board>,
    mut camera: Query<(&mut Transform, &mut OrthographicProjection), With<Camera>>,
) {
    let Ok(mut camera) = camera.get_single_mut() else {
        return;
    };
    let window = board.world_window(2);
    let center = window.center();

    if let ScalingMode::AutoMin {
        min_width,
        min_height,
    } = camera.1.scaling_mode
    {
        camera.0.translation.x *= 0.95;
        camera.0.translation.x += 0.05 * center.x;
        camera.0.translation.y *= 0.95;
        camera.0.translation.y += 0.05 * center.y;
        camera.1.scaling_mode = ScalingMode::AutoMin {
            min_width: 0.95 * min_width + 0.05 * window.width(),
            min_height: 0.95 * min_height + 0.05 * window.height(),
        };
    } else {
        camera.0.translation.x = center.x;
        camera.0.translation.y = center.y;
        camera.1.scaling_mode = ScalingMode::AutoMin {
            min_width: window.width(),
            min_height: window.height(),
        };
    }
}
