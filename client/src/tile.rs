use std::{fmt::Display, mem::transmute, ops::Add};

use bevy::{prelude::*, sprite::Anchor};
use thiserror::Error;

pub(crate) type Int = i16;

pub(crate) const X_AXIS_SCALE: f32 = 1.0;
pub(crate) const Y_AXIS_SCALE: f32 = 0.8;

#[derive(Clone, Copy, Debug)]
pub(crate) enum Layer {
    Board = 0,
    LockMarker = 1,
    TileMarker = 2,
    Piece = 3,
}

impl Layer {
    fn z_offset(self) -> f32 {
        self as u8 as f32 * 0.001
    }

    fn anchor(self) -> Anchor {
        match self {
            Layer::Piece => Anchor::Custom(Vec2::new(0.0, -0.26)),
            _ => Anchor::Center,
        }
    }
}

/// A tile is represented with 3 axes the following way:
/// ```text
///   []qq[]
///  [][][][]
/// [][]00[][]
///  ss[][]rr
///   [][][]
/// ```
/// From this follows, that when moving by one tile to any side,
/// one axis in incremented and one axis is decremented, i.e. r + s + q = 0 always.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Component)]
pub struct Tile([Int; 3]);

impl Tile {
    /// Create a new tile with the given coordinates.
    pub fn new(q: Int, r: Int) -> Tile {
        Tile([q, r, -q - r])
    }

    /// Constructs a new tile from the given coordinates without checking for
    /// q + r + s = 0.
    pub fn from_array_unchecked(coords: [Int; 3]) -> Tile {
        Tile(coords)
    }

    /// Returns the position along the q axis.
    pub fn q(self) -> Int {
        self.0[0]
    }

    /// Returns the position along the r axis.
    pub fn r(self) -> Int {
        self.0[1]
    }

    /// Returns the position along the s axis.
    pub fn s(self) -> Int {
        self.0[2]
    }

    /// Returns the distance to another tile. The distance is defined as the
    /// minimum number of steps to reach one tile from the other.
    pub fn distance(self, other: Tile) -> Int {
        ((self.q() - other.q()).abs() + (self.r() - other.r()).abs() + (self.s() - other.s()).abs())
            / 2
    }

    /// Returns an iterator of tiles around this tile.
    pub fn around(self) -> impl ExactSizeIterator<Item = Tile> + Clone {
        [
            [1, 0, -1],
            [0, 1, -1],
            [-1, 1, 0],
            [-1, 0, 1],
            [0, -1, 1],
            [1, -1, 0],
        ]
        .iter()
        .copied()
        .map(Tile::from_array_unchecked)
        .map(move |offset| self + offset)
    }

    /// Creates a tile from grid coordinates.
    /// ```text
    ///   [][][]
    ///  [][][][]
    /// [][]00[]20 -> x
    ///  [][]01[]
    ///   []02[]
    ///     |
    ///     v
    ///     y
    /// ```
    pub fn from_grid(x: Int, y: Int) -> Tile {
        Tile::new(-y, x + y / 2 + if y < 0 { 0 } else { y % 2 })
    }

    /// Converts a tile to grid coordinates.
    /// ```text
    ///   []0-[]
    ///  [][]0-[]
    /// [][]00[]20 -> x
    ///  [][]01[]
    ///   []02[]
    ///     |
    ///     v
    ///     y
    /// ```
    pub fn to_grid(self) -> (Int, Int) {
        (
            // x = floor(q / 2) + r
            self.q() / 2 + self.r() + if self.q() < 0 { self.q() % 2 } else { 0 },
            // y = q
            -self.q(),
        )
    }

    /// Tries to find the tile corresponding to some world space position.
    pub(crate) fn from_world(x: f32, y: f32) -> Option<Tile> {
        // convert to grid coordinates
        let yi = ((-y / Y_AXIS_SCALE).round()) as Int;
        let xi = ((x - if yi % 2 == 0 { 0.0 } else { 0.5 }) / X_AXIS_SCALE).round() as Int;

        // find tile corresponding to position
        let tile = Tile::from_grid(xi, yi);
        let center = tile.to_world();

        // check elliptical hitbox of tile
        let xd = (center.x - x) / X_AXIS_SCALE;
        let yd = (center.y - y) / Y_AXIS_SCALE;
        let dsq = xd * xd + yd * xd;
        if dsq < 0.4f32.powi(2) {
            Some(tile)
        } else {
            None
        }
    }

    /// Returns the world space position of the center of the tile.
    pub(crate) fn to_world(self) -> Vec2 {
        let (x, y) = self.to_grid();

        let x = (x as f32 + if y % 2 == 0 { 0.0 } else { 0.5 }) * X_AXIS_SCALE;
        let y = -y as f32 * Y_AXIS_SCALE;

        Vec2::new(x, y)
    }

    /// Moves the tile by one step in the direction of the given axes.
    pub fn step(&mut self, direction: Direction) {
        self.0[direction.pos_axis() as usize] += 1;
        self.0[direction.neg_axis() as usize] -= 1;
    }

    /// Returns the next tile in the given direction.
    pub fn neighbor(mut self, direction: Direction) -> Self {
        self.0[direction.pos_axis() as usize] += 1;
        self.0[direction.neg_axis() as usize] -= 1;
        self
    }

    /// Returns an iterator of the tiles in a given direction.
    pub fn walk(mut self, direction: Direction) -> impl Iterator<Item = Tile> {
        (0..).map(move |_| {
            self.step(direction);
            self
        })
    }

    /// Creates a bevy [SpriteBundle] for this tile using the given texture.
    pub(crate) fn sprite_bundle(self, texture: Handle<Image>, layer: Layer) -> SpriteBundle {
        let pos = self.to_world();
        let (x, y) = (pos.x, pos.y);
        SpriteBundle {
            sprite: Sprite {
                anchor: layer.anchor(),
                custom_size: Some(Vec2::new(1.0, 1.0)),
                ..default()
            },
            transform: Transform::from_xyz(x, y, layer.z_offset()),
            texture,
            ..default()
        }
    }
}

impl Add<Tile> for Tile {
    type Output = Tile;

    fn add(self, rhs: Tile) -> Self::Output {
        Tile([self.q() + rhs.q(), self.r() + rhs.r(), self.s() + rhs.s()])
    }
}

impl From<(Int, Int)> for Tile {
    fn from(value: (Int, Int)) -> Self {
        Tile::from_grid(value.0, value.1)
    }
}

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum Error {
    #[error("sum of coordinates is not 0")]
    CoordinateSum,
}

impl TryFrom<(Int, Int, Int)> for Tile {
    type Error = Error;

    fn try_from(value: (Int, Int, Int)) -> Result<Self, Self::Error> {
        if value.0 + value.1 + value.2 == 0 {
            Ok(Tile([value.0, value.1, value.2]))
        } else {
            Err(Error::CoordinateSum)
        }
    }
}

impl Display for Tile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let (x, y) = self.to_grid();
        f.write_fmt(format_args!("Tile({x}, {y})"))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum Axis {
    Q = 0,
    R = 1,
    S = 2,
}

impl Axis {
    /// Returns the axis not given as an argument if the two given axes are different and
    /// `None` otherwise.
    pub fn third(a: Axis, b: Axis) -> Option<Axis> {
        if a == b {
            None
        } else {
            Some(unsafe { transmute(3 - a as u8 - b as u8) })
        }
    }

    /// Returns the axis not given as an argument.
    ///
    /// # Safety
    /// May cause undefined behaviour if the axes given are equal.
    pub unsafe fn third_unchecked(a: Axis, b: Axis) -> Axis {
        unsafe { transmute(3 - a as u8 - b as u8) }
    }
}

/// Represents one of the six directions to move to on the hex grid.
#[derive(Debug, Clone, Copy)]
pub enum Direction {
    TopRight,
    Right,
    BottomRight,
    BottomLeft,
    Left,
    TopLeft,
}

impl Direction {
    /// Returns the axis that gets incremented when moving in this direction.
    pub fn pos_axis(self) -> Axis {
        match self {
            Direction::TopRight => Axis::Q,
            Direction::Right => Axis::R,
            Direction::BottomRight => Axis::R,
            Direction::BottomLeft => Axis::S,
            Direction::Left => Axis::S,
            Direction::TopLeft => Axis::Q,
        }
    }

    /// Returns the axis that gets decremented when moving in this direction.
    pub fn neg_axis(self) -> Axis {
        match self {
            Direction::TopRight => Axis::S,
            Direction::Right => Axis::S,
            Direction::BottomRight => Axis::Q,
            Direction::BottomLeft => Axis::Q,
            Direction::Left => Axis::R,
            Direction::TopLeft => Axis::R,
        }
    }

    /// Returns the axis orthogonal to this direction.
    pub fn const_axis(self) -> Axis {
        match self {
            Direction::TopRight => Axis::R,
            Direction::Right => Axis::Q,
            Direction::BottomRight => Axis::S,
            Direction::BottomLeft => Axis::R,
            Direction::Left => Axis::Q,
            Direction::TopLeft => Axis::S,
        }
    }

    /// Returns an iterator over all directions.
    pub fn all() -> impl ExactSizeIterator<Item = Self> {
        [
            Direction::TopRight,
            Direction::Right,
            Direction::BottomRight,
            Direction::BottomLeft,
            Direction::Left,
            Direction::TopLeft,
        ]
        .into_iter()
    }
}

#[cfg(test)]
mod test {
    use crate::tile::Direction;

    use super::Tile;

    #[test]
    fn walk() {
        assert_eq!(
            Tile::from((0, 0))
                .walk(Direction::Right)
                .take(5)
                .collect::<Vec<_>>(),
            (1..=5).map(|x| Tile::from((x, 0))).collect::<Vec<_>>()
        );

        assert_eq!(
            Tile::from((0, 0))
                .walk(Direction::BottomRight)
                .take(5)
                .collect::<Vec<_>>(),
            Vec::from([(0, 1), (1, 2), (1, 3), (2, 4), (2, 5),].map(Tile::from))
        );

        assert_eq!(
            Tile::from((0, 0))
                .walk(Direction::TopRight)
                .take(5)
                .collect::<Vec<_>>(),
            Vec::from([(0, -1), (1, -2), (1, -3), (2, -4), (2, -5),].map(Tile::from))
        );
    }

    #[test]
    fn add() {
        let a = [(0, 0), (-5, 3), (2, 4), (100, 0), (-25, -25)].map(Tile::from);

        let b = [(0, 0), (2, 2), (-2, -4), (0, -100), (3, -4)].map(Tile::from);

        let results = [(0, 0), (-3, 5), (0, 0), (100, -100), (-22, -29)].map(Tile::from);

        for ((a, b), res) in Iterator::zip(a.into_iter(), b).zip(results) {
            assert_eq!(a + b, res);
        }
    }

    #[test]
    fn around() {
        let tiles = Tile::new(0, 0).around().collect::<Vec<_>>();
        assert_eq!(
            tiles,
            vec![
                Tile::new(1, 0),
                Tile::new(0, 1),
                Tile::new(-1, 1),
                Tile::new(-1, 0),
                Tile::new(0, -1),
                Tile::new(1, -1)
            ],
        )
    }

    #[test]
    fn world_conversion() {
        for tile in [(0, 0), (-1, 0), (3, 2), (0, -5), (10, 10)].map(Tile::from) {
            let world = dbg!(tile.to_world());
            assert_eq!(tile, Tile::from_world(world.x, world.y).unwrap());
        }
    }

    #[test]
    fn grid_conversion() {
        for tile in [
            (0, 0),
            (0, 1),
            (0, -1),
            (1, 0),
            (-1, 0),
            (3, 2),
            (0, -5),
            (10, 10),
        ]
        .map(Tile::from)
        {
            let grid = dbg!(tile.to_grid());
            assert_eq!(tile, Tile::from_grid(grid.0, grid.1));
        }

        let pairs = [
            ((0, 0), (0, 0, 0)),
            ((0, 1), (-1, 1, 0)),
            ((0, 2), (-2, 1, 1)),
            ((0, 3), (-3, 2, 1)),
            ((0, 4), (-4, 2, 2)),
            ((0, -1), (1, 0, -1)),
            ((0, -2), (2, -1, -1)),
            ((0, -3), (3, -1, -2)),
            ((0, -4), (4, -2, -2)),
            ((1, 0), (0, 1, -1)),
            ((2, 0), (0, 2, -2)),
            ((-1, 0), (0, -1, 1)),
            ((-2, 0), (0, -2, 2)),
            ((1, -1), (1, 1, -2)),
            ((2, -2), (2, 1, -3)),
        ]
        .map(|(t1, t2)| (Tile::from(t1), Tile::try_from(t2).unwrap()));

        for (t1, t2) in pairs {
            assert_eq!(t1, t2);
        }
    }
}
