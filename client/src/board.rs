use std::fmt::Display;

use bevy::{
    asset::{AssetServer, Handle},
    ecs::{
        component::Component,
        entity::Entity,
        event::Event,
        system::{Command, Resource},
        world::World,
    },
    math::{Rect, Vec2},
    render::texture::Image,
    utils::{HashMap, HashSet},
};
use itertools::Itertools;
use thiserror::Error;

use crate::tile::{self, Direction, Layer, Tile, X_AXIS_SCALE, Y_AXIS_SCALE};

#[derive(Default, Debug, Clone, Copy, PartialEq, Eq, Component, Resource)]
pub enum Player {
    #[default]
    Red,
    Black,
}

impl Display for Player {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match self {
            Player::Red => "red",
            Player::Black => "black",
        })
    }
}

impl Player {
    pub fn swap(&mut self) {
        match self {
            Player::Red => *self = Player::Black,
            Player::Black => *self = Player::Red,
        }
    }

    pub fn both() -> impl ExactSizeIterator<Item = Self> {
        [Player::Red, Player::Black].into_iter()
    }
}

#[derive(Debug, Clone, Copy, Event, PartialEq, Eq, Hash)]
pub struct Move {
    pub from: Tile,
    pub to: Tile,
}

impl Move {
    /// Create a new Move from two [Tile]s.
    pub fn new(from: Tile, to: Tile) -> Self {
        Self { from, to }
    }
}

#[derive(Debug, Clone, Resource)]
pub struct Board {
    /// Set of tiles that make up the board.
    tiles: HashSet<Tile>,
    /// List of pieces on the board for each player.
    pieces: [HashSet<Tile>; 2],
    /// Tile that was last placed and thus cannot be removed.
    last_placed: Option<Tile>,
}

#[derive(Debug, Clone, Error)]
pub enum Error {
    #[error("an unknown error")]
    Unknown,
    #[error("parity of number of leading spaces is not alternating")]
    MismatchedSpaces,
    #[error("trailing character found")]
    ExtraChar,
    #[error("invalid character pair found")]
    InvalidTile,
    #[error("board contains multiple locked tiles")]
    MultipleLocked,
    #[error("move is not possible")]
    InvalidMove,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum TileKind {
    Red,
    Black,
    Empty,
    Gap,
    Locked,
}

impl Board {
    pub fn new(tiles: HashSet<Tile>, pieces: [HashSet<Tile>; 2], locked: Option<Tile>) -> Board {
        Board {
            tiles,
            pieces,
            last_placed: locked,
        }
    }

    pub fn tiles(&self) -> impl Iterator<Item = Tile> + '_ {
        self.tiles.iter().copied()
    }

    pub fn tile_count(&self) -> usize {
        self.tiles.len()
    }

    pub fn pieces(&self, player: Player) -> impl Iterator<Item = Tile> + '_ {
        self.pieces[player as usize].iter().copied()
    }

    pub fn piece_count(&self, player: Player) -> usize {
        self.pieces[player as usize].len()
    }

    pub fn piece_at(&self, tile: Tile) -> Option<Player> {
        if self.pieces[Player::Red as usize].contains(&tile) {
            Some(Player::Red)
        } else if self.pieces[Player::Black as usize].contains(&tile) {
            Some(Player::Black)
        } else {
            None
        }
    }

    pub fn is_valid(&self, player: Player, mov: Move) -> bool {
        self.player_piece_moves(player).contains(&mov)
            || (self.free_tiles().contains(&mov.from) && self.tile_moves(mov.from).contains(&mov))
    }

    /// Checks whether the given tile blocks piece movements by either being
    /// a gap or having an occupying piece.
    pub fn is_blocking(&self, tile: Tile) -> bool {
        !self.tiles.contains(&tile) || self.piece_at(tile).is_some()
    }

    /// Generates all piece moves possible on the board for the given player.
    pub fn player_piece_moves(&self, player: Player) -> impl Iterator<Item = Move> + '_ {
        self.pieces[player as usize]
            .iter()
            .copied()
            .map(move |tile| self.piece_moves(tile))
            .flatten()
    }

    /// Generates all piece moves possible for a given piece.
    pub fn piece_moves(&self, piece: Tile) -> impl Iterator<Item = Move> + '_ {
        Direction::all().flat_map(move |dir| {
            self.check_piece_move(piece, dir)
                .map(|tile| Move::new(piece, tile))
        })
    }

    /// Returns the furthest the piece can move along the given direction or None if
    /// movement is not possible at all.
    fn check_piece_move(&self, piece: Tile, direction: Direction) -> Option<Tile> {
        piece
            .walk(direction)
            .take_while(|tile| !self.is_blocking(*tile))
            .last()
    }

    /// Generates all tile moves possible for a given tile.
    pub fn tile_moves(&self, tile: Tile) -> impl Iterator<Item = Move> + '_ {
        self.tiles()
            .flat_map(Tile::around)
            .unique()
            .filter(move |tile2| self.is_placeable(tile, *tile2))
            .map(move |tile2| Move::new(tile, tile2))
    }

    pub fn free_tiles(&self) -> impl Iterator<Item = Tile> + '_ {
        self.tiles().filter(|tile| self.is_free(*tile))
    }

    /// Returns true if the board contains the tile and it can be legally removed,
    /// false otherwise.
    pub fn is_free(&self, tile: Tile) -> bool {
        self.contains(tile)
            && self.last_placed != Some(tile)
            && self.piece_at(tile).is_none()
            && tile
                .around()
                .map(|tile| self.contains(tile))
                .circular_tuple_windows::<(_, _)>()
                .any(|(t1, t2)| !t1 && !t2)
    }

    pub fn is_placeable(&self, from: Tile, to: Tile) -> bool {
        !self.contains(to)
            && to
                .around()
                .filter(|tile| *tile != from)
                .filter(|tile| self.contains(*tile))
                .count()
                >= 2
    }

    pub fn contains(&self, tile: Tile) -> bool {
        self.tiles.contains(&tile)
    }

    pub fn locked(&self) -> Option<Tile> {
        self.last_placed
    }

    pub fn victory(&self) -> Option<Player> {
        for player in [Player::Red, Player::Black] {
            let pieces = &self.pieces[player as usize];
            let mut searched = HashSet::default();
            let mut found = Vec::new();

            let initial = *pieces.iter().next().unwrap();
            found.push(initial);
            while let Some(piece) = found.pop() {
                searched.insert(piece);
                found.extend(piece.around().filter(|tile| {
                    self.piece_at(*tile) == Some(player) && !searched.contains(tile)
                }));
            }
            if searched.len() == pieces.len() {
                return Some(player);
            }
        }
        None
    }

    /// Applies the given move to the board.
    pub fn apply(&mut self, mov: Move) -> Result<(), Error> {
        if let Some(player) = self.piece_at(mov.from) {
            if self.piece_moves(mov.from).contains(&mov) {
                self.pieces[player as usize].remove(&mov.from);
                self.pieces[player as usize].insert(mov.to);
            } else {
                return Err(Error::InvalidMove);
            }
        } else {
            if self.is_free(mov.from) && self.is_placeable(mov.from, mov.to) {
                self.tiles.remove(&mov.from);
                self.tiles.insert(mov.to);
                self.last_placed = Some(mov.to);
            } else {
                return Err(Error::InvalidMove);
            }
        }
        Ok(())
    }

    /// Calculates the smallest rectangle in world space the whole board fits in.
    pub fn world_window(&self, padding: u32) -> Rect {
        let mut x = (f32::INFINITY, f32::NEG_INFINITY);
        let mut y = (f32::INFINITY, f32::NEG_INFINITY);
        for tile in &self.tiles {
            let pos = tile.to_world();
            x.0 = x.0.min(pos.x);
            x.1 = x.1.max(pos.x);
            y.0 = y.0.min(pos.y);
            y.1 = y.1.max(pos.y);
        }
        Rect::from_corners(
            Vec2::new(
                x.0 - (padding as f32 + 1.0) * X_AXIS_SCALE,
                y.0 - (padding as f32 + 1.0) * Y_AXIS_SCALE,
            ),
            Vec2::new(
                x.1 + (padding as f32 + 1.0) * X_AXIS_SCALE,
                y.1 + (padding as f32 + 1.2) * Y_AXIS_SCALE,
            ),
        )
    }

    pub fn from_ascii_art(ascii: impl AsRef<str>) -> Result<Self, Error> {
        let mut tiles = HashSet::default();
        let mut pieces = [HashSet::new(), HashSet::new()];
        let mut locked = None;

        let mut even = None;

        // construct vec of lines
        let lines = ascii
            .as_ref()
            .lines()
            .enumerate()
            .map(|(li, l)| {
                // convert line to bytes
                let bytes = l.trim_end().as_bytes();

                // count spaces at start of line
                let spaces = bytes
                    .iter()
                    .take_while(|b| **b == b' ' || **b == b'-')
                    .count();

                // check parity of number of spaces
                let _even = (spaces + li) % 2 == 0;
                if even == Some(!_even) {
                    return Err(Error::MismatchedSpaces);
                } else {
                    even = Some(_even);
                }

                // ignore leading spaces
                let bytes = &bytes[spaces..];

                // count chars
                let n = bytes.len();
                // check if chars can be split into blocks of 2
                if n % 2 != 0 {
                    return Err(Error::ExtraChar);
                }

                // parse blocks of 2 consecutive chars
                let tiles = (0..n / 2)
                    .map(|range| &bytes[(2 * range)..=(2 * range + 1)])
                    .map(|tile| match tile {
                        b"[]" | b"()" => Ok(TileKind::Empty),
                        b"rr" | b"RR" => Ok(TileKind::Red),
                        b"bb" | b"BB" => Ok(TileKind::Black),
                        b"xx" | b"XX" => Ok(TileKind::Locked),
                        b"  " | b"--" => Ok(TileKind::Gap),
                        _ => Err(Error::InvalidTile),
                    })
                    .collect::<Result<Vec<TileKind>, Error>>()?;
                Ok((spaces, tiles))
            })
            .collect::<Result<Vec<(usize, _)>, Error>>()?;

        let found_tiles = lines.iter().enumerate().flat_map(|(li, (spaces, tiles))| {
            // add one space to even lines to make it work even if odd lines
            // have no spaces
            // divide by two to get leading empty tiles
            let gaps = (spaces + 1 - li % 2) / 2;
            tiles.iter().enumerate().map(move |(i, kind)| {
                (
                    Tile::from_grid(gaps as tile::Int + i as tile::Int, li as tile::Int),
                    kind,
                )
            })
        });

        for (tile, kind) in found_tiles {
            match *kind {
                TileKind::Red => {
                    tiles.insert(tile);
                    pieces[Player::Red as usize].insert(tile);
                }
                TileKind::Black => {
                    tiles.insert(tile);
                    pieces[Player::Black as usize].insert(tile);
                }
                TileKind::Empty => {
                    tiles.insert(tile);
                }
                TileKind::Locked => {
                    tiles.insert(tile);
                    if locked.is_none() {
                        locked = Some(tile);
                    } else {
                        return Err(Error::MultipleLocked);
                    }
                }
                TileKind::Gap => {}
            }
        }

        Ok(Board::new(tiles, pieces, locked))
    }

    pub fn to_ascii_art(&self) -> String {
        let (mut minx, mut miny) = (0, 0);
        let mut lines = Vec::new();
        for (x, y) in self.tiles.iter().map(|tile| tile.to_grid()) {
            if x < minx {
                minx = x;
            }
            if y < miny {
                miny = y;
            }
        }
        for (tile, (x, y)) in self.tiles.iter().map(|tile| {
            let (x, y) = tile.to_grid();
            (tile, ((x - minx) as usize, (y - miny) as usize))
        }) {
            if lines.len() < (y + 1) {
                lines.resize_with(y + 1, Vec::new);
            }
            let req_len = (x + 1) * 2 + y % 2;
            if lines[y].len() < req_len {
                lines[y].resize_with(req_len, || b' ');
            }
            let i = x * 2 + y % 2;
            let t = if self.pieces[Player::Red as usize].contains(tile) {
                b"rr"
            } else if self.pieces[Player::Black as usize].contains(tile) {
                b"bb"
            } else if self.last_placed == Some(*tile) {
                b"xx"
            } else {
                b"[]"
            };
            lines[y][i] = t[0];
            lines[y][i + 1] = t[1];
        }
        lines
            .into_iter()
            .map(|line| String::from_utf8(line).unwrap())
            .join("\n")
    }

    pub(crate) fn load(self) -> impl Command {
        |world: &mut World| {
            if let Some(board) = world.remove_resource::<BoardEntities>() {
                board.unload(world);
            }

            let mut tiles = HashMap::default();
            let mut pieces = HashMap::default();
            let mut last_placed = None;

            let asset_server = world.get_resource::<AssetServer>().unwrap();
            let tex_empty: Handle<Image> = asset_server.load("tile.png");
            let tex_piece_red: Handle<Image> = asset_server.load("red.png");
            let tex_piece_black: Handle<Image> = asset_server.load("black.png");
            for tile in self.tiles() {
                let mut board =
                    world.spawn((tile, tile.sprite_bundle(tex_empty.clone(), Layer::Board)));
                if self.last_placed == Some(tile) {
                    board.insert(LastPlaced);
                    last_placed = Some(board.id());
                }
                tiles.insert(tile, board.id());

                if let Some(player) = self.piece_at(tile) {
                    let tex = match player {
                        Player::Red => tex_piece_red.clone(),
                        Player::Black => tex_piece_black.clone(),
                    };
                    let piece = world.spawn((tile, player, tile.sprite_bundle(tex, Layer::Piece)));
                    pieces.insert(tile, piece.id());
                }
            }

            world.insert_resource(BoardEntities {
                tiles,
                pieces,
                last_placed,
            });

            world.insert_resource(self);
        }
    }
}

impl Default for Board {
    fn default() -> Self {
        Self::from_ascii_art(
            "  rr[]bb
 [][][][]
bb[][][]rr
 [][][][]
  rr[]bb",
        )
        .unwrap()
    }
}

#[derive(Resource, Debug)]
pub(crate) struct BoardEntities {
    /// Set of tiles that make up the board.
    pub(crate) tiles: HashMap<Tile, Entity>,
    /// List of pieces on the board for each player.
    pub(crate) pieces: HashMap<Tile, Entity>,
    /// Tile that was last placed and thus cannot be removed.
    pub(crate) last_placed: Option<Entity>,
}

#[derive(Clone, Copy, Debug, Component)]
struct LastPlaced;

impl BoardEntities {
    pub(crate) fn unload(self, world: &mut World) {
        for (_, e) in self.tiles {
            world.despawn(e);
        }
        for (_, e) in self.pieces.iter() {
            world.despawn(*e);
        }
        if let Some(e) = self.last_placed {
            world.despawn(e);
        }
    }
}

#[cfg(test)]
mod test {
    use bevy::utils::HashSet;

    use crate::{
        board::{Board, Move},
        tile::Tile,
    };

    use super::Player;

    #[test]
    fn ascii_art() {
        for ascii in [
            "",
            "[][][]",
            "[]\n []\n[]",
            "  rr[]bb\n [][][][]\nbb[][][]rr\n [][][][]\n  rr[]bb",
            "[][][]xx",
        ] {
            assert_eq!(
                ascii,
                dbg!(Board::from_ascii_art(ascii)).unwrap().to_ascii_art()
            );
        }

        for ascii in [
            "   []",
            " []\n  []\n []",
            "             []          []",
            " [][][]\n[][][]",
            "()()()",
            "()[]()",
            "()xx()",
        ] {
            assert!(Board::from_ascii_art(ascii).is_ok());
        }

        for wrong_ascii in [
            "[]\n[]\n[]",
            "abc",
            "[]][]]",
            "rb",
            "[[[[]]]]",
            "[] []",
            "[]\n  []",
            "()xx()xx",
            "(]",
        ] {
            assert!(dbg!(Board::from_ascii_art(wrong_ascii)).is_err());
        }
    }

    #[test]
    fn piece_moves() {
        let board = Board::default();
        assert_eq!(
            board
                .piece_moves(Tile::from_grid(0, 2))
                .collect::<HashSet<_>>(),
            [(0, 1), (0, 3), (3, 3),]
                .into_iter()
                .map(|tile| Move::new(Tile::from_grid(0, 2), Tile::from(tile)))
                .collect::<HashSet<_>>()
        );
        assert_eq!(board.player_piece_moves(Player::Red).count(), 9);
        assert_eq!(board.player_piece_moves(Player::Black).count(), 9);
    }

    #[test]
    fn tile_moves() {
        let board = Board::default();
        assert_eq!(board.tile_moves(Tile::new(0, 2)).count(), 10);
    }
}
