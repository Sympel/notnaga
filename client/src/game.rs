use bevy::{prelude::*, sprite::Anchor};
use itertools::Itertools;

use crate::{
    board::{Board, BoardEntities, Move, Player},
    input::TileClick,
    tile::{Layer, Tile},
    GameState,
};

pub struct GamePlugin;

impl Plugin for GamePlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_event::<Move>()
            .init_state::<TurnState>()
            .init_resource::<Player>()
            .init_resource::<Selected>()
            .add_systems(Update, handle_player_actions)
            .add_systems(Update, apply_moves)
            .add_systems(Update, markers)
            .add_systems(Update, victory_check);
    }
}

#[derive(Default, Deref, Debug, Clone, Copy, Resource)]
struct Selected(Option<Tile>);

#[derive(Default, Debug, Clone, Copy, Resource, States, Hash, PartialEq, Eq)]
enum TurnState {
    #[default]
    Piece,
    Tile,
}

#[derive(Debug, Clone, Copy, Component)]
struct Marker;

fn victory_check(
    mut commands: Commands,
    board: ResMut<Board>,
    asset_server: Res<AssetServer>,
    camera: Query<&GlobalTransform, With<Camera>>,
    gamestate: Res<State<GameState>>,
    mut next_state: ResMut<NextState<GameState>>,
) {
    let cam = camera.single().translation();
    if *gamestate.get() != GameState::Play {
        return;
    }
    if let Some(player) = board.victory() {
        commands.spawn(SpriteBundle {
            sprite: Sprite {
                anchor: Anchor::Center,
                custom_size: Some(Vec2::new(8.0, 2.0)),
                ..default()
            },
            transform: Transform::from_xyz(cam.x, cam.y, 0.5),
            texture: asset_server.load(&format!("victory_{}.png", player)),
            ..default()
        });
        next_state.set(GameState::Victory);
    }
}

fn handle_player_actions(
    turnstate: Res<State<TurnState>>,
    gamestate: Res<State<GameState>>,
    mut next_state: ResMut<NextState<TurnState>>,
    mut player: ResMut<Player>,
    mut selected: ResMut<Selected>,
    mut board: ResMut<Board>,
    mut clicks: EventReader<TileClick>,
    mut moves: EventWriter<Move>,
) {
    if *gamestate.get() != GameState::Play {
        return;
    }
    let Some(clicked) = clicks.read().next() else {
        return;
    };
    if let Some(from) = selected.0 {
        selected.0 = None;
        let to = **clicked;
        let mov = Move::new(from, to);
        if board.is_valid(*player, mov) {
            board.apply(mov).unwrap();
            moves.send(mov);
            match turnstate.get() {
                TurnState::Piece => {
                    next_state.set(TurnState::Tile);
                }
                TurnState::Tile => {
                    next_state.set(TurnState::Piece);
                    player.swap();
                }
            }
            return;
        }
    }
    let select = match turnstate.get() {
        TurnState::Piece => board.pieces(*player).contains(&**clicked),
        TurnState::Tile => board.free_tiles().contains(&**clicked),
    };
    if select {
        selected.0 = Some(**clicked);
    }
}

fn apply_moves(
    mut board: ResMut<BoardEntities>,
    mut tiles: Query<(&mut Tile, &mut Transform)>,
    mut moves: EventReader<Move>,
) {
    for mov in moves.read() {
        let id;
        if board.pieces.contains_key(&mov.from) {
            id = board.pieces.remove(&mov.from).unwrap();
            board.pieces.insert(mov.to, id);
        } else {
            id = board.tiles.remove(&mov.from).unwrap();
            board.tiles.insert(mov.to, id);
        }
        let (mut tile, mut t) = tiles.get_mut(id).unwrap();
        *tile = mov.to;
        let pos = mov.to.to_world();
        t.translation.x = pos.x;
        t.translation.y = pos.y;
    }
}

fn markers(
    mut commands: Commands,
    turnstate: Res<State<TurnState>>,
    gamestate: Res<State<GameState>>,
    selected: ResMut<Selected>,
    markers: Query<Entity, (With<Tile>, With<Marker>)>,
    board: Res<Board>,
    asset_server: Res<AssetServer>,
) {
    if !selected.is_changed() && !turnstate.is_changed() && !gamestate.is_changed() {
        return;
    }
    for marker in markers.iter() {
        commands.entity(marker).despawn();
    }
    if *gamestate.get() != GameState::Play {
        return;
    }
    if let Some(locked) = board.locked() {
        if board.piece_at(locked).is_none() {
            commands.spawn((
                locked.sprite_bundle(
                    asset_server.load("tile_locked_marker.png"),
                    Layer::LockMarker,
                ),
                locked,
                Marker,
            ));
        }
    }
    match turnstate.get() {
        TurnState::Piece => {
            let Some(piece) = selected.0 else {
                return;
            };
            commands.spawn((
                piece.sprite_bundle(
                    asset_server.load("piece_selected_outline.png"),
                    Layer::Piece,
                ),
                piece,
                Marker,
            ));
            for mov in board.piece_moves(piece) {
                commands.spawn((
                    mov.to.sprite_bundle(
                        asset_server.load("tile_move_target.png"),
                        Layer::TileMarker,
                    ),
                    mov.to,
                    Marker,
                ));
            }
        }
        TurnState::Tile => {
            if let Some(tile) = selected.0 {
                commands.spawn((
                    tile.sprite_bundle(asset_server.load("tile_selected.png"), Layer::TileMarker),
                    tile,
                    Marker,
                ));
                for mov in board.tile_moves(tile) {
                    commands.spawn((
                        mov.to.sprite_bundle(
                            asset_server.load("tile_placeable.png"),
                            Layer::TileMarker,
                        ),
                        mov.to,
                        Marker,
                    ));
                }
            } else {
                for tile in board.free_tiles() {
                    commands.spawn((
                        tile.sprite_bundle(
                            asset_server.load("tile_removeable_marker.png"),
                            Layer::TileMarker,
                        ),
                        tile,
                        Marker,
                    ));
                }
            }
        }
    }
}
