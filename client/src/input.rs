use bevy::{prelude::*, window::PrimaryWindow};

use crate::tile::Tile;

pub struct InputPlugin;

impl Plugin for InputPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_event::<TileClick>()
            .add_systems(Update, handle_mouse_clicks);
    }
}

#[derive(Debug, Deref, Clone, Copy, Event)]
pub struct TileClick(Tile);

fn handle_mouse_clicks(
    mouse: Res<ButtonInput<MouseButton>>,
    window: Query<&Window, With<PrimaryWindow>>,
    camera: Query<(&Camera, &GlobalTransform)>,
    mut clicks: EventWriter<TileClick>,
) {
    let Some(cursor) = window.single().cursor_position() else {
        return;
    };
    let (camera, cam_transform) = camera.single();
    let Some(Some(tile)) = camera
        .viewport_to_world_2d(cam_transform, cursor)
        .map(|pos| Tile::from_world(pos.x, pos.y))
    else {
        return;
    };
    for button in mouse.get_just_pressed() {
        info!("clicked tile {tile:?} with {button:?}");
        clicks.send(TileClick(tile));
    }
}
