use bevy::{prelude::*, window::PrimaryWindow};

pub struct CursorPlugin;

impl Plugin for CursorPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_systems(Startup, setup_cursor)
            .add_systems(Update, track_cursor);
    }
}

#[derive(Debug, Component)]
struct Cursor;

fn setup_cursor(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut primary_window: Query<&mut Window, With<PrimaryWindow>>,
) {
    let mut window = primary_window.single_mut();
    window.cursor.visible = false;
    commands.spawn((
        Cursor,
        ImageBundle {
            image: asset_server.load("cursor.png").into(),
            style: Style {
                position_type: PositionType::Absolute,
                height: Val::Px(24.0),
                width: Val::Px(24.0),
                ..default()
            },
            z_index: ZIndex::Local(1),
            ..default()
        },
    ));
}

fn track_cursor(
    mut cursors: Query<&mut Style, With<Cursor>>,
    primary_window: Query<&Window, With<PrimaryWindow>>,
) {
    if let Some(cursor) = primary_window.single().cursor_position() {
        let mut cursor_style = cursors.single_mut();
        cursor_style.left = Val::Px(cursor.x);
        cursor_style.top = Val::Px(cursor.y);
        cursor_style.right = Val::Auto;
        cursor_style.bottom = Val::Auto;
    }
}
